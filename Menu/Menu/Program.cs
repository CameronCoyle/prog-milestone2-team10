﻿using System;

namespace ConsoleUI
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var menu = 5;

            do
            {
                Console.Clear();


                Console.WriteLine($"******************************");
                Console.WriteLine($"*          TEAM 10 MENU      *");
                Console.WriteLine($"*                            *");
                Console.WriteLine($"*    1. Task One             *");
                Console.WriteLine($"*    2. Task Two             *");
                Console.WriteLine($"*    3. Task Three           *");
                Console.WriteLine($"*    4. Task Four            *");
                Console.WriteLine($"*                            *");
                Console.WriteLine($"******************************");
                Console.WriteLine();
                Console.WriteLine($"Please select a task below!");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();

                    Console.WriteLine($"DATE CALCULATOR");
                    menu = int.Parse(Console.ReadLine());

                    {
                        Console.WriteLine("Please enter your date of birth! (dd/mm/yyyy)");
                        var dob = Console.ReadLine();

                        string birthDateString = $"{dob}";
                        DateTime birthDate;
                        if (DateTime.TryParse(birthDateString, out birthDate))
                        {
                            DateTime today = DateTime.Now;
                            Console.WriteLine("You are {0} days old", (today - birthDate).Days);
                        }
                        else Console.WriteLine("Incorrect date format!");
                    }
                }

                if (menu == 2)
                {
                    Console.Clear();

                    Console.WriteLine($"CALCULATE GRADE AVERAGE");
                    menu = int.Parse(Console.ReadLine());
                }

                if (menu == 3)
                {
                    Console.Clear();

                    Console.WriteLine($"GENERATE RANDOM NUMBEr");
                    

                    public static void Main2(string[] args)
        {
                        Generator();
                    }


                    static void Generator()
       {
                        int Score = 0;
                        for (int i = 1; i <= 5; i++)
                        {

                            Console.WriteLine("Enter a whole number between 1-5");
                            int x = Convert.ToInt32(Console.ReadLine());

                            Random random = new Random();
                            int randomNumber = random.Next(1, 6);

                            Console.WriteLine(randomNumber);

                            if (x == randomNumber)
                            {
                                Console.WriteLine("you got it right");
                                Score++;
                            }

                            else
                            {
                                Console.WriteLine("Sorry you guessed wrong");
                            }
                        }
                        Console.Write("Your score is: ");
                        Console.WriteLine(Score);
                        Console.WriteLine("Do you want to play again? 1 for yes /  2 for no");
                        int answer = Convert.ToInt32(Console.ReadLine());

                        if (answer == 1)
                        {
                            NewGame(Score);
                        }

                        else
                        {
                            Console.WriteLine("Thanks for playing");
                        }
                    }

                    static void NewGame(int Score)
       {
                        Console.Write("Last time you got: ");
                        Console.WriteLine(Score);

                        Score = 0;
                        for (int i = 1; i <= 5; i++)
                        {

                            Console.WriteLine("Enter a whole number between 1-5");
                            int x = Convert.ToInt32(Console.ReadLine());

                            Random random = new Random();
                            int randomNumber = random.Next(1, 6);

                            Console.WriteLine(randomNumber);

                            if (x == randomNumber)
                            {
                                Console.WriteLine("you got it right");
                                Score++;
                            }

                            else
                            {
                                Console.WriteLine("Sorry you guessed wrong");
                            }
                        }
                        Console.WriteLine(Score);
                        Console.WriteLine("Do you want to play again? 1 for yes /  2 for no");
                        int answer = Convert.ToInt32(Console.ReadLine());

                        if (answer == 1)
                        {
                            NewGame(Score);
                        }

                        else
                        {
                            Console.WriteLine("Thanks for playing");
                        }
                }
        }

                if (menu == 4)
                {
                    Console.Clear();

                    Console.WriteLine($"RATE FAVOURITE FOOD");
                    var temp = Console.ReadLine();

                    if (temp == "5")
                    {
                        menu = int.Parse(temp);
                    }
                    else if (temp != "5")
                    {
                        if (temp == "a")
                        {
                            Console.Clear();
                            Console.WriteLine($"This is the result of option 09");
                            Console.WriteLine($"Press <5> to go back to the main menu.");
                            menu = int.Parse(Console.ReadLine());
                        }
                    }

                }

            } while (menu == 5);

            if (menu > 5)
            {
                Console.WriteLine($"Please choose the correct option");
                Console.WriteLine($"Press <ENTER> to return to the main menu");
                Console.ReadLine();
                menu = 5;
                Console.Clear();
            }
        }
    }
